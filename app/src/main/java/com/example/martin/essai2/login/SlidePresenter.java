package com.example.martin.essai2.login;


import com.example.martin.essai2.base.BasePresenter;
import com.example.martin.essai2.base.BaseView;

class SlidePresenter<V extends SlideView & BaseView> extends BasePresenter<V> implements ISlidePresenter<V> {
    SlidePresenter(V view) {
        super(view);
    }

    @Override
    public void onViewSetupFinish() {
        getView().initListeners();
    }

    @Override
    public void onNextBtnClicked() {
        getView().gotoNextScreen();
    }

    @Override
    public void onPrevBtnClicked() {
        getView().gotoPrevScreen();
    }

    @Override
    public void onLoginBtnClicked() {
        getView().openSignInActivity();
    }

    @Override
    public void onCreateAccountBtnClicked() {
        getView().openCreateAccountActivity();
    }

}