package com.example.martin.essai2.base;

import android.support.annotation.StringRes;

public interface BaseView {

    void error(@StringRes int resId);

    void error(String message);

    void success(String message);

    void success(@StringRes int resId);

    boolean isNetworkConnected();

}
