package com.example.martin.essai2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static com.example.martin.essai2.MainEurofrancActivity.EXTRA_MESSAGE;

public class Main2Activity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "eurocfa.tp1.MESSAGE";
    public final static int TAUX = 656;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String params = intent.getStringExtra(MainEurofrancActivity.EXTRA_MESSAGE);
        int idx = params.indexOf(":");
        int len = params.length();
        String val1 =  params.substring(0, idx);
        String op = params.substring(idx+1, idx+3);
        String val2 =  params.substring(idx+4, len);
        String result;
        Intent intent2 = new Intent(this, MainEurofrancActivity.class);
        String params2;
        if (op.equals(new String("ec"))){
            result = convert(val1, op);
            params2   = new String(val1+";"+result);

        } else {
            result = convert(val2, op);
            params2   = new String(result+";"+val2);
        }
        intent2.putExtra(EXTRA_MESSAGE, params2);
        startActivity(intent2);
        finish();
    }

    public String convert(String v, String o) {
        //la conversion Euro/ CFA sera de 1 Euro pour 656 CFA voir constante TAUX
        double res;
        if (o.equals(new String("ec"))) { // conversion vers CFA
            Double d = new Double(v);
            res = d.doubleValue();
            res*=TAUX;
        } else { // conversion vers EURO
            Double d = new Double(v);
            res = d.doubleValue();
            res/=TAUX;
        }
        NumberFormat nf = new DecimalFormat("0.##");
        String s = nf.format(res);
        return s;
    }
}
