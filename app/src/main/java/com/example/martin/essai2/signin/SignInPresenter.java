package com.example.martin.essai2.signin;

import android.text.TextUtils;

import com.example.martin.essai2.base.BasePresenter;
import com.example.martin.essai2.base.BaseView;


class SignInPresenter<V extends SignInView & BaseView> extends BasePresenter<V> implements ISignInPresenter<V> {
    SignInPresenter(V view) {
        super(view);
    }


    @Override
    public void attemptLogin(String login, String password) {
        if (TextUtils.isEmpty(login)) {
            getView().loginFieldError();
        } else if (TextUtils.isEmpty(password)) {
            getView().passwordFieldError();
        } else {
            getView().openMainActivity();
        }
    }

    @Override
    public void onSignUpBtnClicked() {
        getView().openSignUpActivity();
    }

    @Override
    public void onSignInBtnClicked() {
        getView().login();
    }
}
