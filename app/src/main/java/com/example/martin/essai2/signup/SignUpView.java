package com.example.martin.essai2.signup;


import com.example.martin.essai2.base.BaseView;

/**
 * Created by steve.tchatchouang on 04/10/2017
 */

interface SignUpView extends BaseView {
    void showFirstStepFragment();

    void finish();

    void showSecondStepFragment(String fName, String lName, long birthday, String gender);

    void callOnBackPressed();

    void firstFragmentShowGenderBox();

    void firstFragmentShowBirthdayBox();
}
