package com.example.martin.essai2.login;


import com.example.martin.essai2.base.BaseView;
import com.example.martin.essai2.base.IBasePresenter;

interface ISlidePresenter<V extends SlideView & BaseView> extends IBasePresenter<V> {

    void onViewSetupFinish();

    void onNextBtnClicked();

    void onPrevBtnClicked();

    void onLoginBtnClicked();

    void onCreateAccountBtnClicked();
}