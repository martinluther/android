package com.example.martin.essai2.signup;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.martin.essai2.R;
import com.example.martin.essai2.base.BaseActivity;


public class SignUpActivity extends BaseActivity implements SignUpView, FirstStepFragment.FirstFragmentListener,
        SecondStepFragment.SecondFragmentListener {

    Toolbar toolbar;
    private FirstStepFragment mFirstStepFragment;
    private SecondStepFragment mSecondStepFragment;

    private SignUpPresenter<SignUpView> mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mPresenter = new SignUpPresenter<SignUpView>(this);

        setUp();
        if (savedInstanceState == null) {
            mPresenter.onActivityCreated();
        }
    }

    @Override
    protected void setUp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void showFirstStepFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (mFirstStepFragment == null) {
            mFirstStepFragment = (FirstStepFragment) fm.findFragmentByTag(FirstStepFragment.TAG);
            if (mFirstStepFragment == null) {
                mFirstStepFragment = FirstStepFragment.newInstance();
            }
        }
        ft.add(R.id.container, mFirstStepFragment, FirstStepFragment.TAG);
        ft.commit();
    }


    @Override
    public void showSecondStepFragment(final String fName, final String lName, final long birthday, final String gender) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);
        if (mSecondStepFragment == null) {
            mSecondStepFragment = (SecondStepFragment) fm.findFragmentByTag(SecondStepFragment.TAG);
            if (mSecondStepFragment == null) {
                mSecondStepFragment = SecondStepFragment.newInstance();
            }
        }
        ft.replace(R.id.container, mSecondStepFragment, SecondStepFragment.TAG);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void callOnBackPressed() {
        onBackPressed();
    }

    @Override
    public void firstFragmentShowGenderBox() {
        mFirstStepFragment.showGenderDialog();
    }

    @Override
    public void firstFragmentShowBirthdayBox() {
        mFirstStepFragment.showBirthdayCalendar();
    }

    @Override
    public void onContinueBtnClicked(String fName, String lName, long birthday, String gender) {
        mPresenter.onContinueBtnClicked(fName, lName, birthday, gender);
    }

    @Override
    public void onCancelBtnClicked() {
        mPresenter.onFirstFragmentCancelClicked();
    }

    @Override
    public void onGenderEdtClicked() {
        mPresenter.onFirstFragmentEdtGenderClicked();
    }

    @Override
    public void onBirthdayEdtClicked() {
        mPresenter.onFirstFragmentBirthdayEdtClicked();
    }

    @Override
    public void onPrevBtnClicked() {
        mPresenter.onSecondFragmentPrevBtnClicked();
    }
}
