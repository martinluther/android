package com.example.martin.essai2.login;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.martin.essai2.R;


public class SlideTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View page, float position) {
        int pagePosition = (int) page.getTag();
        int pageWidth = page.getWidth();
        float pageWidthTimesPosition = pageWidth * position;
        float absPosition = Math.abs(position);
        if (!(position <= -1.0f || position >= 1.0f)) {
            View description = page.findViewById(R.id.description);
            description.setTranslationY(-pageWidthTimesPosition / 2f);
            description.setAlpha(1.0f - absPosition);
            View image = page.findViewById(R.id.image);
            if (pagePosition >= 0 && image != null) {
                image.setAlpha(1.0f - absPosition);
                image.setTranslationX(-pageWidthTimesPosition * 1.5f);
            }
        }
    }
}