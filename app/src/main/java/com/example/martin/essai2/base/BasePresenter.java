
package com.example.martin.essai2.base;

import android.os.Handler;

public class BasePresenter<V extends BaseView> implements IBasePresenter<V> {

    private V mView;
    private Handler handler;

    public BasePresenter(V view) {
        this.mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    @Override
    public V getView() {
        return mView;
    }

    @Override
    public boolean isViewAttached() {
        return mView != null;
    }

    @Override
    public void post(Runnable runnable) {
        if (runnable == null)
            return;
        handler.post(runnable);
    }

    @Override
    public void postWithDelay(Runnable runnable, long delay) {
        if (runnable == null)
            return;
        handler.postDelayed(runnable, delay);
    }
}
