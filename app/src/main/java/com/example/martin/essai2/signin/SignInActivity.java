package com.example.martin.essai2.signin;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.martin.essai2.Main2Activity;
import com.example.martin.essai2.R;
import com.example.martin.essai2.base.BaseActivity;
import com.example.martin.essai2.signup.SignUpActivity;


public class SignInActivity extends BaseActivity implements SignInView, View.OnClickListener {
    EditText loginField;
    EditText passwordField;
    Button signUpButton;
    Button btnSignIn;


    SignInPresenter<SignInView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_signin);
        mPresenter = new SignInPresenter<SignInView>(this);
        loginField = (EditText) findViewById(R.id.login);
        passwordField = (EditText) findViewById(R.id.password);
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        signUpButton = (Button) findViewById(R.id.sign_up_button);
        setUp();
    }

    @Override
    public void login() {
        mPresenter.attemptLogin(loginField.getText().toString(), passwordField.getText().toString());
    }

    @Override
    public void openSignUpActivity() {
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void openMainActivity() {
        navigateActivity(Main2Activity.class);
    }

    @Override
    public void loginFieldError() {
        fieldError(loginField, getString(R.string.enter_your_login));
    }

    @Override
    public void passwordFieldError() {
        fieldError(passwordField, getString(R.string.enter_your_password));
    }

    private void fieldError(EditText field, String errorMessage) {
        field.setError(errorMessage);
        field.requestFocus();
    }

    @Override
    public void loginSuccess() {
        navigateActivity(Main2Activity.class);
        finish();
    }

    @Override
    public void setUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setTitle(null);
        }
        passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL || id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_ACTION_GO) {
                    login();
                    return true;
                }
                return false;
            }
        });


        btnSignIn.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null)
            mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up_button:
                mPresenter.onSignUpBtnClicked();
                break;
            case R.id.btn_sign_in:
                mPresenter.onSignInBtnClicked();
                break;
        }
    }
}
