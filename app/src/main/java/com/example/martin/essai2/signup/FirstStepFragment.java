package com.example.martin.essai2.signup;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;


import com.example.martin.essai2.R;
import com.example.martin.essai2.base.BaseFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FirstStepFragment extends BaseFragment implements View.OnClickListener {

    EditText edtFirstName;
    EditText edtLastName;
    EditText edtGender;
    EditText edtBirthday;

    private long birthday;

    private FirstFragmentListener mListener;
    public static final String TAG = FirstStepFragment.class.getSimpleName();

    public static FirstStepFragment newInstance() {
        Bundle args = new Bundle();
        FirstStepFragment fragment = new FirstStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        birthday = 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FirstFragmentListener) {
            mListener = (FirstFragmentListener) context;
        } else {
            throw new IllegalArgumentException(context.toString() + " must implement FirstFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first_step, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtFirstName = (EditText) view.findViewById(R.id.edt_first_name);
        edtLastName = (EditText) view.findViewById(R.id.edt_last_name);
        edtGender = (EditText) view.findViewById(R.id.edt_gender);
        edtBirthday = (EditText) view.findViewById(R.id.edt_birthday);

        edtGender.setOnClickListener(this);
        edtBirthday.setOnClickListener(this);
        view.findViewById(R.id.btn_continue).setOnClickListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    void showBirthdayCalendar() {
        final Calendar c = Calendar.getInstance();
        DatePickerDialog dp = new DatePickerDialog(
                getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        c.set(Calendar.YEAR, i);
                        c.set(Calendar.MONTH, i1);
                        c.set(Calendar.DAY_OF_MONTH, i2);
                        setBirthday(c.getTimeInMillis());
                    }
                },
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        );
        dp.show();
    }

    private void setBirthday(long timeInMillis) {
        birthday = timeInMillis;
        edtBirthday.setText(SimpleDateFormat.getDateInstance().format(new Date(timeInMillis)));
    }

    void showGenderDialog() {
        final String[] items = {getString(R.string.male), getString(R.string.female)};
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle(R.string.gender);
        b.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        b.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_gender:
                mListener.onGenderEdtClicked();
                break;
            case R.id.edt_birthday:
                mListener.onBirthdayEdtClicked();
                break;
            case R.id.btn_continue:
                mListener.onContinueBtnClicked(
                        edtFirstName.getText().toString(),
                        edtLastName.getText().toString(),
                        birthday,
                        edtGender.getText().toString()
                );
                break;
            case R.id.btn_cancel:
                mListener.onCancelBtnClicked();
                break;
        }
    }

    interface FirstFragmentListener {
        void onContinueBtnClicked(String fName, String lName, long birthday, String gender);

        void onCancelBtnClicked();

        void onGenderEdtClicked();

        void onBirthdayEdtClicked();
    }
}
