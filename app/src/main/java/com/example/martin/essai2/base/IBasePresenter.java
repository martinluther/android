package com.example.martin.essai2.base;

public interface IBasePresenter<V extends BaseView> {

    void onDetach();

    V getView();

    boolean isViewAttached();

    void postWithDelay(Runnable runnable, long delay);

    void post(Runnable runnable);
}
