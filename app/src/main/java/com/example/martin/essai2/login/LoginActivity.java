package com.example.martin.essai2.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.martin.essai2.Main2Activity;
import com.example.martin.essai2.R;
import com.example.martin.essai2.base.BaseActivity;
import com.example.martin.essai2.signin.SignInActivity;
import com.example.martin.essai2.signup.SignUpActivity;


public class LoginActivity extends BaseActivity implements LoginView, SlideView, View.OnClickListener {

    ViewPager viewpager;
    Button prevBtn;
    ImageView[] dots;
    Button nextBtn;
    private SlidePresenter<SlideView> mPresenter;
    private Handler mHandler;
    private Runnable mRunnable;
    private SlideAdapter slideAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
        setContentView(R.layout.activity_login);
        mPresenter = new SlidePresenter<SlideView>(this);
        prevBtn = (Button) findViewById(R.id.prev_btn);
        nextBtn = (Button) findViewById(R.id.next_btn);
        dots = new ImageView[]{(ImageView) findViewById(R.id.dot1), (ImageView) findViewById(R.id.dot2), (ImageView) findViewById(R.id.dot3),
                (ImageView) findViewById(R.id.dot4), (ImageView) findViewById(R.id.dot5)};
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        setUp();
    }

    @Override
    protected void setUp() {
        slideAdapter = new SlideAdapter(getSupportFragmentManager());
        viewpager.setAdapter(slideAdapter);
        viewpager.setPageTransformer(false, new SlideTransformer());
        mPresenter.onViewSetupFinish();
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                mPresenter.onNextBtnClicked();
                mHandler.postDelayed(mRunnable, 5000);
            }
        };
        findViewById(R.id.create_account_btn).setOnClickListener(this);
        findViewById(R.id.login_btn).setOnClickListener(this);
        findViewById(R.id.next_btn).setOnClickListener(this);
        findViewById(R.id.prev_btn).setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunnable, 5000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public void openMainActivity() {
        startActivity(new Intent(this, Main2Activity.class));
        finish();
    }

    @Override
    public void gotoNextScreen() {
        int nextPosition = viewpager.getCurrentItem() + 1;
        if (nextPosition == slideAdapter.getCount()) {
            nextPosition = 0;
        }
        viewpager.setCurrentItem(nextPosition, true);
    }

    @Override
    public void gotoPrevScreen() {
        int nextPosition = viewpager.getCurrentItem() - 1;
        if (nextPosition < 0) {
            nextPosition = viewpager.getChildCount();
        }
        viewpager.setCurrentItem(nextPosition, true);
    }

    @Override
    public void initListeners() {
        viewpager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                uncheckedAllDots();
                dots[position].setImageDrawable(ContextCompat.getDrawable(
                        LoginActivity.this,
                        R.drawable.checked_indicator)
                );
            }
        });
    }

    @Override
    public void openSignInActivity() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @Override
    public void openCreateAccountActivity() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void terminate() {
        finish();
    }

    private void uncheckedAllDots() {
        for (ImageView dot : dots) {
            dot.setImageDrawable(ContextCompat.getDrawable(
                    LoginActivity.this,
                    R.drawable.unchecked_indicator)
            );
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prev_btn:
                mPresenter.onPrevBtnClicked();
                break;
            case R.id.next_btn:
                mPresenter.onNextBtnClicked();
                break;
            case R.id.login_btn:
                mPresenter.onLoginBtnClicked();
                break;
            case R.id.create_account_btn:
                mPresenter.onCreateAccountBtnClicked();
        }
    }
}
