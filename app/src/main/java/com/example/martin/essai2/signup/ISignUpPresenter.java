package com.example.martin.essai2.signup;

import android.graphics.Bitmap;

import com.example.martin.essai2.base.BaseView;
import com.example.martin.essai2.base.IBasePresenter;


interface ISignUpPresenter<V extends SignUpView & BaseView> extends IBasePresenter<V> {
    void onActivityCreated();

    void onContinueBtnClicked(String fName, String lName, long birthday, String gender);

    void onFirstFragmentCancelClicked();

    void onFirstFragmentEdtGenderClicked();

    void onFirstFragmentBirthdayEdtClicked();

    void onSecondFragmentPrevBtnClicked();
}
