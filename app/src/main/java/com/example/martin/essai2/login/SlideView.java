package com.example.martin.essai2.login;


import com.example.martin.essai2.base.BaseView;

interface SlideView extends BaseView {
    void openMainActivity();

    void gotoNextScreen();

    void gotoPrevScreen();

    void initListeners();

    void openSignInActivity();

    void openCreateAccountActivity();

    void terminate();
}