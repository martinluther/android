package com.example.martin.essai2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainEurofrancActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "eurocfa.tp1.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_eurofranc);

        Button btnEC = (Button)findViewById(R.id.btnEC);
        btnEC.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        euro2Cfa(v);
                    }
                }
        );

        Button btnCE = (Button)findViewById(R.id.btnCE);
        btnCE.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Cfa2euro(v);
                    }
                }
        );


    }

    public void euro2Cfa(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        String txt1 = ((EditText) findViewById(R.id.txtDev1)).getText().toString();
        if (txt1.equals(""))
            txt1+="0";
        String txt2 = ((EditText) findViewById(R.id.txtDev2)).getText().toString();
        if (txt2.equals(""))
            txt2+="0";
        String params   = new String(txt1+":ec;"+txt2);
        intent.putExtra(EXTRA_MESSAGE, params);
        startActivity(intent);
        finish();
    }

    public void Cfa2euro(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        String txt1 = ((EditText) findViewById(R.id.txtDev1)).getText().toString();
        if (txt1.equals(""))
            txt1+="0";
        String txt2 = ((EditText) findViewById(R.id.txtDev2)).getText().toString();
        if (txt2.equals(""))
            txt2+="0";
        String params   = new String(txt1+":ce;"+txt2);
        intent.putExtra(EXTRA_MESSAGE, params);
        startActivity(intent);
        finish();
    }

    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        String params = intent.getStringExtra(Main2Activity.EXTRA_MESSAGE);
        if (params != null){ // on vient de la seconde activity
            int idx = params.indexOf(";");
            int len = params.length();
            String val1 =  params.substring(0, idx);
            String val2 = params.substring(idx+1, len);
            ((EditText) findViewById(R.id.txtDev1)).setText(val1);
            ((EditText) findViewById(R.id.txtDev2)).setText(val2);
        }
    }
}
