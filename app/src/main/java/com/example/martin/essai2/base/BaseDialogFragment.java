package com.example.martin.essai2.base;

import android.support.v4.app.DialogFragment;

import android.content.Context;
import android.support.annotation.StringRes;

public abstract class BaseDialogFragment extends DialogFragment implements BaseView {
    private BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mActivity = (BaseActivity) context;
        } else
            throw new IllegalArgumentException("Activity must be child of BaseActivity");
    }

    @Override
    public void error(String message) {
        if (mActivity != null) mActivity.error(message);
    }

    @Override
    public void error(@StringRes int res) {
        if (mActivity != null) mActivity.error(res);
    }

    @Override
    public void success(String message) {
        if (mActivity != null) mActivity.success(message);
    }

    @Override
    public void success(@StringRes int res) {
        if (mActivity != null) mActivity.success(res);
    }

    @Override
    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }
}