package com.example.martin.essai2.login;


import com.example.martin.essai2.base.BaseView;
import com.example.martin.essai2.base.IBasePresenter;

interface ILoginPresenter<V extends LoginView & BaseView> extends IBasePresenter<V> {

}
