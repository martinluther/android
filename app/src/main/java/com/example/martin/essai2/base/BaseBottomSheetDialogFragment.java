package com.example.martin.essai2.base;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetDialogFragment;

/**
 * Created by steve.tchatchouang on 02/10/2017
 */

public abstract class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment implements BaseView {
    private BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mActivity = (BaseActivity) context;
        } else
            throw new IllegalArgumentException("Activity must be child of BaseActivity");
    }

    @Override
    public void error(String message) {
        if (mActivity != null) mActivity.error(message);
    }

    @Override
    public void error(@StringRes int res) {
        if (mActivity != null) mActivity.error(res);
    }

    @Override
    public void success(String message) {
        if (mActivity != null) mActivity.success(message);
    }

    @Override
    public void success(@StringRes int res) {
        if (mActivity != null) mActivity.success(res);
    }

    @Override
    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }
}
