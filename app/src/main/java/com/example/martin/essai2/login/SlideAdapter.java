package com.example.martin.essai2.login;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SlideAdapter extends FragmentPagerAdapter {

    SlideAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SlideFragment.newInstance(Color.parseColor("#8352A5"), position);
            case 1:
                return SlideFragment.newInstance(Color.parseColor("#009688"), position);
            case 2:
                return SlideFragment.newInstance(Color.parseColor("#03a9f4"), position);
            case 3:
                return SlideFragment.newInstance(Color.parseColor("#83a944"), position);
            case 4:
                return SlideFragment.newInstance(Color.parseColor("#0a9f4c"), position);
            default:
                throw new IllegalArgumentException("Unknown position : "+position);
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
