package com.example.martin.essai2.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.martin.essai2.R;
import com.example.martin.essai2.base.BaseFragment;


public class SecondStepFragment extends BaseFragment implements View.OnClickListener {

    private SecondFragmentListener mListener;
    public static final String TAG = SecondStepFragment.class.getSimpleName();

    public static SecondStepFragment newInstance() {
        Bundle args = new Bundle();
        SecondStepFragment fragment = new SecondStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            throw new IllegalArgumentException("Bundle must not be null");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_sign_up).setOnClickListener(this);
        view.findViewById(R.id.btn_prev).setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SecondFragmentListener) {
            mListener = (SecondFragmentListener) context;
        } else {
            throw new IllegalArgumentException(context.toString() + " must implement SecondFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second_step, container, false);
    }

    public void onViewClicked(View view) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up:
                success("TODO");
                break;
            case R.id.btn_prev:
                mListener.onPrevBtnClicked();
                break;
        }
    }

    interface SecondFragmentListener {
        void onPrevBtnClicked();
    }
}
