package com.example.martin.essai2.signin;


import com.example.martin.essai2.base.BaseView;
import com.example.martin.essai2.base.IBasePresenter;

interface ISignInPresenter<V extends SignInView & BaseView> extends IBasePresenter<V> {
    void attemptLogin(String login, String password);

    void onSignUpBtnClicked();

    void onSignInBtnClicked();
}
