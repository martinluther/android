package com.example.martin.essai2.signup;

import android.graphics.Bitmap;

import com.example.martin.essai2.base.BasePresenter;
import com.example.martin.essai2.base.BaseView;


class SignUpPresenter<V extends SignUpView & BaseView> extends BasePresenter<V> implements ISignUpPresenter<V> {

    SignUpPresenter(V view) {
        super(view);
    }

    @Override
    public void onActivityCreated() {
        getView().showFirstStepFragment();
    }

    @Override
    public void onContinueBtnClicked(final String fName, final String lName, final long birthday, final String gender) {
        getView().showSecondStepFragment(fName,lName, birthday, gender);
    }

    @Override
    public void onFirstFragmentCancelClicked() {
        getView().finish();
    }

    @Override
    public void onFirstFragmentEdtGenderClicked() {
        getView().firstFragmentShowGenderBox();
    }

    @Override
    public void onFirstFragmentBirthdayEdtClicked() {
        getView().firstFragmentShowBirthdayBox();
    }


    @Override
    public void onSecondFragmentPrevBtnClicked() {
        getView().callOnBackPressed();
    }
}
