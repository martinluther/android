package com.example.martin.essai2.signin;


import com.example.martin.essai2.base.BaseView;

interface SignInView extends BaseView {
    void loginFieldError();

    void passwordFieldError();

    void loginSuccess();

    void login();

    void openSignUpActivity();

    void openMainActivity();
}
